$(document).ready(function(){
    // $(".main_svg").load('images/svg.svg');

    // youtube modal video
        var idVideo = '';
        $(document).on( 'click', '.jsBtnVideoModal', function (e) {
            e.preventDefault();
            idVideo = $(this).attr('data-idVideo');
            $('.jsModalVideo').modal('show');
        })
        $('.jsModalVideo').on('show.bs.modal', function (e) {
            youtubePlayer.loadVideoById(idVideo, 0, "large")
            youtubePlayVideo()
        })
        $('.jsModalVideo').on('hidden.bs.modal', function (e) {
            youtubeStopVideo()
        })
    // END youtube modal video

    // scroll page
        $(window).scroll(function() {
            if ($(this).scrollTop() > 100) {
                $('.scrollup').fadeIn();
            } else {
                $('.scrollup').fadeOut();
            }
        });
        $('.scrollup').click(function() {
            $("html, body").animate({
                scrollTop: 0
            }, 300);
            return false;
        });
    // END scroll page
});


/*animations on scroll - temp solution (hope :-)*/
(function($){
    $.fn.scrollzip = function(options){
        var settings = $.extend({
            showFunction    : null,
            hideFunction    : null,
            showShift       : 0,
            wholeVisible    : false,
            hideShift       : 0,
        }, options);
        return this.each(function(i,obj){
            $(this).addClass('scrollzip');
            if ( $.isFunction( settings.showFunction ) ){
                if(
                    !$(this).hasClass('isShown')&&
                    ($(window).outerHeight()+$('#scrollzipPoint').offset().top-settings.showShift)>($(this).offset().top+((settings.wholeVisible)?$(this).outerHeight():0))&&
                    ($('#scrollzipPoint').offset().top+((settings.wholeVisible)?$(this).outerHeight():0))<($(this).outerHeight()+$(this).offset().top-settings.showShift)
                ){
                    $(this).addClass('isShown');
                    settings.showFunction.call( this );
                }
            }
            if ( $.isFunction( settings.hideFunction ) ){
                if(
                    $(this).hasClass('isShown')&&
                    (($(window).outerHeight()+$('#scrollzipPoint').offset().top-settings.hideShift)<($(this).offset().top+((settings.wholeVisible)?$(this).outerHeight():0))||
                    ($('#scrollzipPoint').offset().top+((settings.wholeVisible)?$(this).outerHeight():0))>($(this).outerHeight()+$(this).offset().top-settings.hideShift))
                ){
                    $(this).removeClass('isShown');
                    settings.hideFunction.call( this );
                }
            }
            return this;
        });
    };


    // scrollzip-animate.css
        $(window).on("load scroll resize", function(){
            $('.jszoomInFooter').scrollzip({
                showFunction    : function() { $(this).css("visibility", "visible").addClass('zoomIn'); }
            });
            $('.jsbounceInLeft').scrollzip({
                showFunction    : function() { $(this).css("visibility", "visible").addClass('bounceInLeft'); }
            });
            $('.jsbounceInRight').scrollzip({
                showFunction    : function() { $(this).css("visibility", "visible").addClass('bounceInRight'); }
            });
            $('.jsfadeInUp').scrollzip({
                showFunction    : function() { $(this).css("visibility", "visible").addClass('fadeInUp'); }
            });
            $('.jsfadeInDown').scrollzip({
                showFunction    : function() { $(this).css("visibility", "visible").addClass('fadeInDown'); }
            });
            $('.jsfadeInLeft').scrollzip({
                showFunction    : function() { $(this).css("visibility", "visible").addClass('fadeInLeft'); }
            });
            $('.jsfadeInRight').scrollzip({
                showFunction    : function() { $(this).css("visibility", "visible").addClass('fadeInRight'); }
            });
            $('.jszoomIn').scrollzip({
                showFunction    : function() { $(this).css("visibility", "visible").addClass('zoomIn'); }
            });
        });
    // END scrollzip-animate.css
})(jQuery);