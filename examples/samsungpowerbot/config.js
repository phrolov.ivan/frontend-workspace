// path for folder/files
var devPaths = {
  allCss: 'scss/bower.scss',
  scss: 'scss/',
  css: 'css/',
  scripts: 'js/',
  images: 'images/',
  fonts: 'fonts/',
  videos: 'videos/',
  html: '',
  headerFolder: '',
  headerTpl: 'index.html'
}
var distPaths = {
  root: 'dist/',
  css: 'dist/css/',
  scripts: 'dist/js/',
  images: 'dist/images/',
  fonts: 'dist/fonts/',
  videos: 'dist/videos/',
  html: 'dist/',
  headerFolder: 'dist/',
  headerTpl: 'dist/index.html'
}


// browserSync
var sync = {
  serverboolean: false,
  server: {
    files: ['{template-parts}/**/*.php', '*.php'],
    proxy: 'http://',
    snippetOptions: {
      whitelist: ['/wp-admin/admin-ajax.php'],
      blacklist: ['/wp-admin/**']
    }
  },
  noserver: {
    server: {
      baseDir: "",
      routes: {"/bower_components": "bower_components"}
    }
  }
}


// change paths, exclude files from bower
var bowerFiles = {
  fileTypes: {
      html: {
        replace: {
          // js: '<script src="/wp-content/themes/{{filePath}}"></script>',
          // css: '<link rel="stylesheet" href="/wp-content/themes/{{filePath}}" />'
        }
      }
    },
    exclude: [
      '/bower_components/tether/dist/js/tether.js',
      // '/bower_components/bootstrap/dist/js/bootstrap.js'
    ]
}


// custom bootstrap
var bowerBootstrapSrc = 'bower_components/bootstrap/js/dist/'
var bowerBootstrapDist = 'bower_components/bootstrap/dist/js/'
var bootstrapComponents = [
  bowerBootstrapSrc + 'util.js',
  // bowerBootstrapSrc + 'alert.js',
  // bowerBootstrapSrc + 'button.js',
  // bowerBootstrapSrc + 'carousel.js',
  bowerBootstrapSrc + 'collapse.js',
  // bowerBootstrapSrc + 'dropdown.js',
  bowerBootstrapSrc + 'modal.js',
  // bowerBootstrapSrc + 'popover.js',
  // bowerBootstrapSrc + 'scrollspy.js',
  // bowerBootstrapSrc + 'tab.js',
  // bowerBootstrapSrc + 'tooltip.js'
]


// combine files
var useref = {//on real projects with backend may be issues
  transformPath: function(filePath) {//this is example how to fix paths when files combining
    return filePath.replace('/wp-content/','../../')
  }
}


// remove css
var purifyCssSrc = [
  devPaths.html + '**/*.html',
  distPaths.scripts + '**/*.js'
]


// critical css
var removeAttrPages = distPaths.headerTpl;
var criticalSrcPages = [//links to pages and related critical css
    {
      url: 'index.html',
      css: 'index.html',
      include: [],
    }
]
var critical = {
  base: 'dist/',
  inline: true,
  ignore: ['@font-face',/url\(/, /.modal/, /*/.dropdown/*/],
  css: [
    'css/bower.css',
    'css/style.css'
  ],
  minify: true,
  timeout: 3000000,
  width: 1300,
  height: 750
}


// autoprefixer
var settingsAutoprefixer = {
  browsers: [
    'last 2 versions',
    'safari >= 3.1',
    'android 4'
  ]
}


// service links
var serviceLinks = {
  url: devPaths.headerTpl,
  links01: 
    '\n' +
    '<!-- build:css css/all.css -->\n' +
    '<link rel="stylesheet" href="css/bower.css">\n' +
    '<!-- bower:css -->\n' +
    '<!-- endbower -->\n' +
    '<link rel="stylesheet" href="css/style.css">\n' +
    '<!-- endbuild -->\n' +
    '\n',
  script: 
    '<script>!function(e){"use strict";var t=function(t,n,r){function o(e){if(i.body)return e();setTimeout(function(){o(e)})}function a(){d.addEventListener&&d.removeEventListener("load",a),d.media=r||"all"}var l,i=e.document,d=i.createElement("link");if(n)l=n;else{var s=(i.body||i.getElementsByTagName("head")[0]).childNodes;l=s[s.length-1]}var u=i.styleSheets;d.rel="stylesheet",d.href=t,d.media="only x",o(function(){l.parentNode.insertBefore(d,n?l:l.nextSibling)});var f=function(e){for(var t=d.href,n=u.length;n--;)if(u[n].href===t)return e();setTimeout(function(){f(e)})};return d.addEventListener&&d.addEventListener("load",a),d.onloadcssdefined=f,f(a),d};"undefined"!=typeof exports?exports.loadCSS=t:e.loadCSS=t}("undefined"!=typeof global?global:this),function(e){if(e.loadCSS){var t=loadCSS.relpreload={};if(t.support=function(){try{return e.document.createElement("link").relList.supports("preload")}catch(e){return!1}},t.poly=function(){for(var t=e.document.getElementsByTagName("link"),n=0;n<t.length;n++){var r=t[n];"preload"===r.rel&&"style"===r.getAttribute("as")&&(e.loadCSS(r.href,r,r.getAttribute("media")),r.rel=null)}},!t.support()){t.poly();var n=e.setInterval(t.poly,300);e.addEventListener&&e.addEventListener("load",function(){t.poly(),e.clearInterval(n)}),e.attachEvent&&e.attachEvent("onload",function(){e.clearInterval(n)})}}}(this);</script>' +
    '\n',
  links02: 
    '<!-- build:js js/all.js defer -->\n' +
    '<!-- bower:js -->\n' +
    '<!-- endbower -->\n' +
    '<script src="js/script.js"></script>\n' +
    '<!-- endbuild -->\n' +
    '</head>'
}


module.exports = {
    removeAttrPages, removeAttrPages,
    criticalSrcPages, criticalSrcPages,
    critical: critical,
    bootstrapComponents: bootstrapComponents,
    bowerBootstrapDist: bowerBootstrapDist,
    bowerFiles: bowerFiles,
    devPaths: devPaths,
    distPaths: distPaths,
    purifyCssSrc: purifyCssSrc,
    serviceLinks: serviceLinks,
    settingsAutoprefixer: settingsAutoprefixer,
    sync: sync,
    useref: useref
}