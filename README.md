<h1>!!!WARNING!!! THIS IS DEPRECATED - PLEASE USE https://gitlab.com/phrolov.ivan/frontend-workspace-static FOR STATIC!</h1>

<h1>About</h1>
Workspace for frontend:
* download and inject dependencies (css/js/components) (**b.**)
* watch changes (**h.**) on fly and livereload pages (**g.**)
* js: generate custom bootstrap.js (i.e. keep only used modules on project **d.**), concatenation all js files into one minified file (**i.**)
* css: compilation scss to css with vendor prefixes (**c.**), concatenation all css files into one minified file (**i.** and **m.**), extracts & inlines critical (above-the-fold) CSS to html (**n.**)
* images: image optimization (**k.**), convertation png/jpg into webp (**e.**)
<br>



<h1>Quick start<sup>*</sup></h1>
1. `npm i` - install npm packages (required one time PER PROJECT)
2. `bower i` - install bower dependencies (required one time PER PROJECT)
3. `gulp` - run default task (development), `gulp production` - production for to test, `gulp production_live` - production server.
 <br>   * step 0: must be installed https://nodejs.org/, npm install gulp -g, npm install bower -g - install nodejs, gulp&bower globally on machine (in case if it has not been installed yet - required one time PER MACHINE).


<h1>bower.json</h1>
Download and inject dependencies (css/js/components) (**b.**).

For add/remove any dependencies needs just to change this file and after run `bower i` - this task download dependencies, task `gulp`, `gulp production` or `gulp production_live` inject to project.<br>
Dependencies (css/js) injects between related comments in the header:<br>
```html
<!-- bower:css -->
<!-- endbower -->
<!-- bower:js -->
<!-- endbower -->
```
scss files will inject to the bower.scss


<h1>gulpfile.js</h1>
**Common tasks** - includes to all groups:<br>
**a.** `clean:dist` - clean production (dist) folder  
**b.** `bower` - inject js/css/scss  
**c.** `sass` - compilation scss to css  
**d.** `customBootstrap` - generate custom bootstrap.js - i.e. keep only used modules on project  
**e.** `imageToWebp` - convertation png/jpg into webp  

**Development only tasks** (`gulp`)  
**f.** `firsttask` - replace <gulpHead> in header with  
```html
<!-- build:css css/all.css --><br>
<link rel="stylesheet" href="css/bower.css"><br>
<!-- bower:css -->
<!-- endbower -->
<link rel="stylesheet" href="css/style.css">
<!-- endbuild -->
<!-- build:js js/all.js defer -->
<!-- bower:js -->
<!-- endbower -->
<script src="js/script.js"></script>
<!-- endbuild -->
```
that's structure of css/js for bower and for optimization. That's task needed only one time - when project creates  
**g.** `browserSync` - livereload pages on changes  
**h.** `watch` - watch changes template/js/scss/images  

**Production only tasks** (`gulp production`)  
**i.** `useref` - concatenation all css/js files into one minified file  
**j.** `jsNoCombined` - copy separatelly files into dist (needed when you have js files not for all pages)  
**k.** `images` - image optimization  
**l.** `fonts` - copy fonts into dist  
**m.** `minify` - minify css files  
**n.** `critical` - extracts & inlines critical (above-the-fold) CSS to html  


**Production for live only task** (`gulp production_live`)  
**o.** `generateHeaderLive` - remove <meta name="robots" content="noindex,follow"> from code  

